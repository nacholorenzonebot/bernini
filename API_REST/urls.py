from django.conf.urls import url,include
from rest_framework.urlpatterns import format_suffix_patterns
from API_REST import views

urlpatterns = [
    url(r'^zapato/$', views.ZapatoList.as_view()),
    url(r'^zapato/(?P<pk>[0-9]+)/$', views.ZapatoDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)