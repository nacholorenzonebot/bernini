from API_REST.models import Zapato
from API_REST.serializers import ZapatoSerializer
from rest_framework import generics

class ZapatoList(generics.ListCreateAPIView):
    queryset = Zapato.objects.all()
    serializer_class = ZapatoSerializer

class ZapatoDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Zapato.objects.all()
    serializer_class = ZapatoSerializer