from rest_framework import serializers
from API_REST.models import Zapato

class ZapatoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Zapato
        fields = ('id','marca','talla','precio')
