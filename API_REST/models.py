from __future__ import unicode_literals
from django.db import models

class Zapato(models.Model):
    marca = models.CharField(max_length=50)
    talla = models.IntegerField()
    precio = models.FloatField()

    class Meta:
        ordering = ('marca','talla',)

    def __str__(self):
        return self.marca