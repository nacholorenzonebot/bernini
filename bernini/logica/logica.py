# -*- coding: utf_8 -*-
import smtplib
from email.mime.multipart import MIMEMultipart
from email import encoders
from email.mime.text import MIMEText
from email.header import Header
from openpyxl import *
from openpyxl.writer.excel import save_virtual_workbook
import io
from email.mime.base import MIMEBase
from os.path import basename

emailServer = 'smtp.gmail.com'
emailServerPort = 587
username = "zapateriabernini@gmail.com"
password = "bernini01"
emailfrom = "zapateriabernini@gmail.com"
COMMASPACE = ', '

to = ['lorenzonebot@gmail.com']

def generarExcel(listaZapatos):
    wb = Workbook()
    ws1 = wb.active
    ws1.cell(row=1, column=1).value = 'MARCA'
    ws1.cell(row=1, column=2).value = 'TALLA'
    ws1.cell(row=1, column=3).value = 'PRECIO'
    row = 2
    for zapato in listaZapatos:
        ws1.cell(row=row, column=1).value = zapato['marca']
        ws1.cell(row=row, column=2).value = zapato['talla']
        ws1.cell(row=row, column=3).value = zapato['precio']
        row = row + 1

    enviarEmail(to, "Nueva Compra", "Nueva compra realizada", "nuevacompra.xlsx",wb)

def enviarEmail(recipients, subject, body, filename,file=None):
    try:
        emailto = recipients
        recipient_str = COMMASPACE.join(recipients)
        htmlcode = "<html><body>"+body+"</body></html>"
        msg = MIMEMultipart()
        msg["From"] = emailfrom
        msg["To"] = recipient_str
        msg["Subject"] = Header(subject, 'utf-8')
        msg.preamble = subject

        part2 = MIMEText(htmlcode.encode('latin'), 'html')

        ctype = "application/octet-stream"
        maintype, subtype = ctype.split("/", 1)
        outbuf = io.BytesIO(save_virtual_workbook(file))
        attachment = MIMEBase(maintype, subtype)
        attachment.set_payload(outbuf.getvalue())
        outbuf.close()
        encoders.encode_base64(attachment)
        attachment.add_header("Content-Disposition", "attachment", filename=basename(filename))

        msg.attach(part2)
        msg.attach(attachment)

        server = smtplib.SMTP(emailServer + ":" + str(emailServerPort))
        server.starttls()
        server.login(username, password)
        server.sendmail(emailfrom, emailto, msg.as_string())
        server.quit()
        return True
    except Exception, e:
        print "Error enviando correo: " + str(e)
        return False