# -*- coding: iso-8859-1 -*-
from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from logica.logica import generarExcel
import requests

def login_user(request):
    if request.POST:
        username = str(request.POST['username']).lower()
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/home')
        else:
            return render(request,'login.html')
    return render(request, 'login.html')

@login_required(login_url='/login/')
def home(request):
    response = requests.get('http://localhost:8000/zapato')
    listaZapatos = response.json()
    return render(request, 'home.html', {'listaZapatos': listaZapatos})

@login_required(login_url='/login/')
def compraFinalizada(request, listaZapatos=None):
    listaZapatosComprados = []
    listaZapatos = listaZapatos.split(',')
    for zapato in listaZapatos:
        listaZapatosComprados.append(requests.get('http://localhost:8000/zapato/' + zapato ).json())
    generarExcel(listaZapatosComprados)
    return render(request, 'final.html')