from django.conf.urls import url,include
from django.contrib import admin
from views import login_user,home,compraFinalizada

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('API_REST.urls')),
    url(r'^login/$', login_user),
    url(r'^home/$', home),
    url(r'^finalizado/(?P<listaZapatos>([\w|\W., ]+))$', compraFinalizada),
]
